from django.urls import path
from . import views

app_name = 'stars'
urlpatterns = [
    path('', views.index, name='index'),
    path('list/', views.list, name='list'),
    path('add/', views.create, name='create'),

]