from django.db import models

# Create your models here.

class Contact(models.Model):
    name = models.CharField(max_length=70)
    email = models.EmailField()

    def __str__(self):
        return '%s %s' % (self.name, self.email)
