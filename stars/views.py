from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse

from stars.ContactForm import ContactForm
from stars.models import Contact


def index(request):

    return render(request, 'stars/index.html')

def list(request):

    contact_list = Contact.objects.all()

    context = {'contacts': contact_list}

    return render(request, 'stars/list.html', context)


def create(request):

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            h = Contact()
            h.name = data['name']
            h.email = data['email']
            h.save()

        return HttpResponseRedirect(reverse('stars:list'))


    else:
        form = ContactForm()
    return render(request, 'stars/create.html', {'form': form})