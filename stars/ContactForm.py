from django import forms
from django.core.validators import validate_email

from stars.models import Contact


class ContactForm(forms.Form):


    name = forms.CharField(required=True, max_length=70,  label='Name',
                    widget=forms.TextInput(attrs={'placeholder': 'John Doe', 'class':'form-control'}))


    email = forms.EmailField(required=True,  label='Email',
                    widget=forms.EmailInput(attrs={'placeholder': 'akoh@domain.com', 'class':'form-control'}))
